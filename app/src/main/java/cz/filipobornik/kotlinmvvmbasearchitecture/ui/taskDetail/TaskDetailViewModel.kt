package cz.filipobornik.kotlinmvvmbasearchitecture.ui.taskDetail

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import cz.filipobornik.kotlinmvvmbasearchitecture.data.TaskRepository
import cz.filipobornik.kotlinmvvmbasearchitecture.data.model.Task
import cz.filipobornik.kotlinmvvmbasearchitecture.ui.base.BaseViewModel
import cz.filipobornik.kotlinmvvmbasearchitecture.data.model.others.Resource
import cz.filipobornik.kotlinmvvmbasearchitecture.ui.base.ErrorViewModel
import kotlinx.coroutines.launch

class TaskDetailViewModel(
        private val taskId: Int,
        private val repository: TaskRepository
) : ErrorViewModel() {

    override val errors by lazy {
        repository.errors
    }

    fun getTaskDetail(taskId: Int): LiveData<Task> {
        return repository.getTask(taskId)
    }

    fun fetchTaskDetail() = scope.launch {
        repository.fetchTask(taskId)
    }
}