package cz.filipobornik.kotlinmvvmbasearchitecture.ui.main.tasksListFragment

import cz.filipobornik.kotlinmvvmbasearchitecture.data.TaskRepository
import cz.filipobornik.kotlinmvvmbasearchitecture.data.model.Task
import cz.filipobornik.kotlinmvvmbasearchitecture.ui.base.ErrorViewModel
import kotlinx.coroutines.launch

class TasksListViewModel(
        private val repository: TaskRepository
) : ErrorViewModel() {

    val tasks by lazy {
        repository.getAllTasks()
    }

    override val errors by lazy {
        repository.errors
    }

    fun getTask(taskId: Int) {

    }

    fun fetchNewTasks() = scope.launch {
        repository.fetchNewTasks()
    }

    fun updateTask(task: Task) = scope.launch {
        repository.updateTask(task)
    }

}