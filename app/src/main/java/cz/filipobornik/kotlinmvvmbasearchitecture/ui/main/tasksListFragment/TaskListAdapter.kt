package cz.filipobornik.kotlinmvvmbasearchitecture.ui.main.tasksListFragment

import android.content.Context
import android.view.View
import android.view.ViewGroup
import cz.filipobornik.kotlinmvvmbasearchitecture.R
import cz.filipobornik.kotlinmvvmbasearchitecture.data.model.Task
import cz.filipobornik.kotlinmvvmbasearchitecture.ui.base.recyclerview.BaseViewHolder
import cz.filipobornik.kotlinmvvmbasearchitecture.ui.base.recyclerview.GenericRecyclerViewAdapter
import cz.filipobornik.kotlinmvvmbasearchitecture.ui.base.recyclerview.OnRecyclerViewTaskItemClickListener
import kotlinx.android.synthetic.main.item_task.view.*

class TaskListAdapter(
        context: Context, listener: OnRecyclerViewTaskItemClickListener<Task>
) : GenericRecyclerViewAdapter<Task, OnRecyclerViewTaskItemClickListener<Task>, TaskListAdapter.TaskViewHolder>(
        context, listener
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        return TaskViewHolder(inflate(R.layout.item_task, parent), listener)
    }

    class TaskViewHolder(
            itemView: View,
            listener: OnRecyclerViewTaskItemClickListener<Task>?
    ) : BaseViewHolder<Task, OnRecyclerViewTaskItemClickListener<Task>>(itemView, listener) {

        private val checkBoxTask = itemView.checkBoxItemTaskCompleted
        private val txtViewTitle = itemView.txtViewTaskDetailTitle

        override fun onBind(item: Task) {
            checkBoxTask.isChecked = item.completed
            txtViewTitle.text = item.title
            checkBoxTask.setOnClickListener {
                item.completed = !item.completed
                getListener()?.onItemCheckChange(item, item.completed)
            }
            itemView.setOnClickListener { getListener()?.onItemClicked(item) }
        }

    }
}