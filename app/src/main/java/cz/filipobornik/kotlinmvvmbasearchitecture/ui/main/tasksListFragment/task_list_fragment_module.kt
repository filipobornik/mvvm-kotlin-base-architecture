package cz.filipobornik.kotlinmvvmbasearchitecture.ui.main.tasksListFragment

import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

const val TASK_LIST_FRAGMENT_SCOPE_ID = "TASK_LIST_FRAGMENT_SCOPE_ID"
const val TASK_LIST_RECYCLER_VIEW_ITEM_CLICK_LISTENER = "TASK_LIST_RECYCLER_VIEW_ITEM_CLICK_LISTENER"

val taskListFragmentModule = module {
    viewModel { TasksListViewModel(  get() ) }
    scope(TASK_LIST_FRAGMENT_SCOPE_ID) { TaskListAdapter(androidApplication(), getProperty(TASK_LIST_RECYCLER_VIEW_ITEM_CLICK_LISTENER)) }
}