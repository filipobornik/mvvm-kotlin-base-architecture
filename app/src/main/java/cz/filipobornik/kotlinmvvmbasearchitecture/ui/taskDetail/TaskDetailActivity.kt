package cz.filipobornik.kotlinmvvmbasearchitecture.ui.taskDetail

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import cz.filipobornik.kotlinmvvmbasearchitecture.R
import cz.filipobornik.kotlinmvvmbasearchitecture.data.model.Task
import cz.filipobornik.kotlinmvvmbasearchitecture.di.TASK_DETAIL_VIEW_MODEL_TASK_ID
import cz.filipobornik.kotlinmvvmbasearchitecture.ui.base.BaseActivity
import cz.filipobornik.kotlinmvvmbasearchitecture.ui.base.ErrorActivity
import kotlinx.android.synthetic.main.activity_task_detail.*
import org.koin.android.ext.android.setProperty
import org.koin.android.viewmodel.ext.android.viewModel

class TaskDetailActivity : ErrorActivity() {

    override val viewModel by viewModel<TaskDetailViewModel>()
    private var taskDetail: Task? = null

    private val taskDetailId by lazy {
        intent.getIntExtra(EXTRA_TASK_ID, -1)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setProperty(TASK_DETAIL_VIEW_MODEL_TASK_ID, taskDetailId)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task_detail)

        initObservers()
        viewModel.fetchTaskDetail()
    }

    private fun initObservers() {
        observeTaskDetail()
    }

    private fun observeTaskDetail() {
        viewModel.getTaskDetail(taskDetailId).observe(this, Observer {
            taskDetail = it
            fillViewsWithContent()
        })
    }

    private fun fillViewsWithContent() {
        txtViewTaskDetailTitle.text = taskDetail?.title
        checkBoxTaskDetailCompleted.isChecked = taskDetail?.completed ?: false
    }

    private fun shareTaskDetail() {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, taskDetail?.shareString)
        startActivity(Intent.createChooser(shareIntent, resources.getString(R.string.taskdetail_chooser_share)))
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.taskdetail_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.actionShare -> shareTaskDetail()
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    companion object {

        private const val EXTRA_TASK_ID = "EXTRA_TASK_ID"

        fun newInstance(context: Context, id: Int): Intent {
            val intent = Intent(context, TaskDetailActivity::class.java)
            intent.putExtra(EXTRA_TASK_ID, id)
            return intent
        }
    }
}
