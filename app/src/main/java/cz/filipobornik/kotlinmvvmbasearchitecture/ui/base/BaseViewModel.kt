package cz.filipobornik.kotlinmvvmbasearchitecture.ui.base

import android.arch.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

abstract class BaseViewModel : ViewModel() {

    private val job = Job()
    protected val scope: CoroutineScope = CoroutineScope(Dispatchers.Default + job)

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }

}