package cz.filipobornik.kotlinmvvmbasearchitecture.ui.base

import cz.filipobornik.kotlinmvvmbasearchitecture.common.SingleLiveEvent

abstract class ErrorViewModel() : BaseViewModel() {

    abstract val errors: SingleLiveEvent<Throwable>

}