package cz.filipobornik.kotlinmvvmbasearchitecture.ui.base

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import cz.filipobornik.kotlinmvvmbasearchitecture.R
import cz.filipobornik.kotlinmvvmbasearchitecture.utils.exceptions.NoInternetConnectionException
import cz.filipobornik.kotlinmvvmbasearchitecture.utils.extensions.showMessage
import retrofit2.HttpException

abstract class ErrorActivity : BaseActivity() {

    abstract override val viewModel: ErrorViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeErrors()
    }

    private fun observeErrors() {
        viewModel.errors.observe(this, Observer {
            hideLoading()
            handleError(it)
        })
    }

    protected open fun hideLoading() {}

}