package cz.filipobornik.kotlinmvvmbasearchitecture.ui.base

import android.support.v7.app.AppCompatActivity
import cz.filipobornik.kotlinmvvmbasearchitecture.R
import cz.filipobornik.kotlinmvvmbasearchitecture.utils.exceptions.NoInternetConnectionException
import cz.filipobornik.kotlinmvvmbasearchitecture.utils.extensions.showMessage
import retrofit2.HttpException

abstract class BaseActivity : AppCompatActivity() {

    abstract val viewModel: BaseViewModel

    open fun handleError(throwable: Throwable?) {
        when(throwable) {
            is HttpException -> showMessage(getHttpExceptionErrorMessage(throwable))
            is NoInternetConnectionException -> showMessage(R.string.dialog_no_internet_connection)
            is NullPointerException -> showMessage(R.string.no_data_available)
            else -> showMessage(R.string.unknown_error)
        }
    }

    private fun getHttpExceptionErrorMessage(exception: HttpException): String {
        return when (exception.code()) {
            // TODO add more possible request results
            403 -> resources.getString(R.string.error403)
            404 -> resources.getString(R.string.error404)
            500 -> resources.getString(R.string.error500)
            else -> resources.getString(R.string.unknown_error)
        }
    }

}
