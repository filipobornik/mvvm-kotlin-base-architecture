package cz.filipobornik.kotlinmvvmbasearchitecture.ui.base.recyclerview

interface OnRecyclerViewTaskItemClickListener<T> : BaseRecyclerViewListener {

    fun onItemCheckChange(task: T, checked: Boolean)

    fun onItemClicked(task: T)


}