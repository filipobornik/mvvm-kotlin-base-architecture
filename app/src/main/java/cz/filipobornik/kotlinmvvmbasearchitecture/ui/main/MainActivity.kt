package cz.filipobornik.kotlinmvvmbasearchitecture.ui.main

import android.os.Bundle
import cz.filipobornik.kotlinmvvmbasearchitecture.R
import cz.filipobornik.kotlinmvvmbasearchitecture.ui.base.BaseActivity
import cz.filipobornik.kotlinmvvmbasearchitecture.ui.main.tasksListFragment.TasksListFragment
import cz.filipobornik.kotlinmvvmbasearchitecture.utils.extensions.replaceFragment
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity() {

    override val viewModel by viewModel<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        replaceFragment(R.id.containerMain) { TasksListFragment.newInstance() }
    }
}
