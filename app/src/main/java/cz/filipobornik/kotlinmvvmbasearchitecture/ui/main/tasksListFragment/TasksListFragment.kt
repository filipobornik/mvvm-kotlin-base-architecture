package cz.filipobornik.kotlinmvvmbasearchitecture.ui.main.tasksListFragment

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cz.filipobornik.kotlinmvvmbasearchitecture.R
import cz.filipobornik.kotlinmvvmbasearchitecture.data.model.Task
import cz.filipobornik.kotlinmvvmbasearchitecture.ui.base.ErrorFragment
import cz.filipobornik.kotlinmvvmbasearchitecture.ui.base.recyclerview.OnRecyclerViewTaskItemClickListener
import cz.filipobornik.kotlinmvvmbasearchitecture.ui.taskDetail.TaskDetailActivity
import kotlinx.android.synthetic.main.fragment_tasks_list.*
import org.koin.android.ext.android.get
import org.koin.android.ext.android.setProperty
import org.koin.android.scope.ext.android.bindScope
import org.koin.android.scope.ext.android.getOrCreateScope
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * A simple [Fragment] subclass.
 *
 */
class TasksListFragment : ErrorFragment(), OnRecyclerViewTaskItemClickListener<Task> {

    override val viewModel by viewModel<TasksListViewModel>()
    private val adapter by lazy { get<TaskListAdapter>() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        bindScope(getOrCreateScope(TASK_LIST_FRAGMENT_SCOPE_ID))
        setProperty(TASK_LIST_RECYCLER_VIEW_ITEM_CLICK_LISTENER, this)

        return inflater.inflate(R.layout.fragment_tasks_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showLoading()
        initObservers()
        initRecyclerView()
        initListeners()
    }

    private fun initRecyclerView() {
        recyclerViewTaskList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerViewTaskList.adapter = adapter
    }

    private fun initListeners() {
        swipeRefreshTaskList.setOnRefreshListener { viewModel.fetchNewTasks() }
    }

    private fun initObservers() {
        observeTasks()
        observeErrors()
    }

    private fun observeTasks() {
        viewModel.tasks.observe(this, Observer {
            adapter.updateItems(it ?: emptyList())
            hideLoading()
        })
    }

    override fun onItemClicked(task: Task) {
        startActivity(TaskDetailActivity.newInstance(baseActivity.baseContext, task.id))
    }

    override fun onItemCheckChange(task: Task, checked: Boolean) {
        viewModel.updateTask(task)
    }

    override fun hideLoading() {
        progressBarTaskList.visibility = View.GONE
        swipeRefreshTaskList.isRefreshing = false
    }

    private fun showLoading() {
        progressBarTaskList.visibility = View.VISIBLE
    }

    companion object {

        fun newInstance(): Fragment {
            return TasksListFragment()
        }
    }

}
