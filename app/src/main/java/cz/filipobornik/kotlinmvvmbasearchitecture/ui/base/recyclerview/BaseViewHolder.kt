package cz.filipobornik.kotlinmvvmbasearchitecture.ui.base.recyclerview

import android.support.v7.widget.RecyclerView
import android.view.View

abstract class BaseViewHolder<T, L : BaseRecyclerViewListener>(
        itemView: View,
        private val listener: L?
) : RecyclerView.ViewHolder(itemView) {

    abstract fun onBind(item: T)

    fun getListener(): L? {
        return listener
    }

}