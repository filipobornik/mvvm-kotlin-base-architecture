package cz.filipobornik.kotlinmvvmbasearchitecture.ui.main

import cz.filipobornik.kotlinmvvmbasearchitecture.data.TaskRepository
import cz.filipobornik.kotlinmvvmbasearchitecture.ui.base.BaseViewModel

class MainViewModel(
        private val repository: TaskRepository
) : BaseViewModel() {

}