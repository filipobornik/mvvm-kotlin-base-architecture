package cz.filipobornik.kotlinmvvmbasearchitecture.ui.base.recyclerview

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.View


abstract class GenericRecyclerViewAdapter<T, L : BaseRecyclerViewListener, VH : BaseViewHolder<T, L>>(
        private val context: Context,
        var listener: L?,
        private var items: ArrayList<T> = arrayListOf()
) : RecyclerView.Adapter<VH>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    abstract override fun onCreateViewHolder(p0: ViewGroup, p1: Int): VH

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(viewHolder: VH, position: Int) {
        if (items.isEmpty()) return
        val item = items[position]
        viewHolder.onBind(item)
    }

    fun updateItems(items: List<T>) {
        this.items = items as ArrayList<T>
        notifyDataSetChanged()
    }

    fun getItems(): List<T> {
        return items
    }

    fun addItem(item: T) {
        this.items.add(item)
        notifyDataSetChanged()
    }

    fun getItem(position: Int): T? {
        return if (items.size < position) items[position] else null
    }

    fun clearItems() {
        items.clear()
        notifyDataSetChanged()
    }

    fun removeItem(item: T) {
        if (!items.contains(item)) return

        items.remove(item)
        notifyDataSetChanged()
    }

    fun isEmpty(): Boolean {
        return items.isEmpty()
    }

    protected fun inflate(@LayoutRes layout: Int, parent: ViewGroup?, attachToRoot: Boolean): View {
        return layoutInflater.inflate(layout, parent, attachToRoot)
    }

    protected fun inflate(@LayoutRes layout: Int, parent: ViewGroup?): View {
        return inflate(layout, parent, false)
    }

}