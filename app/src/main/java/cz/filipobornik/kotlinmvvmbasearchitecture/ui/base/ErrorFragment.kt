package cz.filipobornik.kotlinmvvmbasearchitecture.ui.base

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.View

abstract class ErrorFragment : BaseFragment() {

    abstract override val viewModel: ErrorViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeErrors()
    }

    protected open fun observeErrors() {
        viewModel.errors.observe(this, Observer {
            hideLoading()
            baseActivity.handleError(it)
        })
    }

    protected open fun hideLoading() { }

}