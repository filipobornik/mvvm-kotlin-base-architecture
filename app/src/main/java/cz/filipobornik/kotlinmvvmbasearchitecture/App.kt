package cz.filipobornik.kotlinmvvmbasearchitecture

import android.app.Application
import cz.filipobornik.kotlinmvvmbasearchitecture.di.applicationModules
import org.koin.android.ext.android.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin(this, applicationModules)
    }

}