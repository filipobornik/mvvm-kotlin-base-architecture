package cz.filipobornik.kotlinmvvmbasearchitecture.di

import cz.filipobornik.kotlinmvvmbasearchitecture.data.TaskRepository
import cz.filipobornik.kotlinmvvmbasearchitecture.ui.main.MainViewModel
import cz.filipobornik.kotlinmvvmbasearchitecture.ui.main.tasksListFragment.taskListFragmentModule
import cz.filipobornik.kotlinmvvmbasearchitecture.ui.taskDetail.TaskDetailViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

const val TASK_DETAIL_VIEW_MODEL_TASK_ID = "taskDetailViewModelTaskId"

val appModule = module {
    viewModel { MainViewModel(get()) }
    viewModel { TaskDetailViewModel( getProperty(TASK_DETAIL_VIEW_MODEL_TASK_ID), get() ) }

    single { TaskRepository(get(), get(), get()) }
}

val applicationModules = listOf(
        appModule,
        remoteDatasourceModule,
        localDatasourceModule,
        taskListFragmentModule
)