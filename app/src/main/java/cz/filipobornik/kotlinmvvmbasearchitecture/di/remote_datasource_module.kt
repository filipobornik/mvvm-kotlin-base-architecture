package cz.filipobornik.kotlinmvvmbasearchitecture.di

import android.content.Context
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import cz.filipobornik.kotlinmvvmbasearchitecture.BuildConfig
import cz.filipobornik.kotlinmvvmbasearchitecture.data.remote.InternetConnectionInterceptor
import cz.filipobornik.kotlinmvvmbasearchitecture.data.remote.TasksService
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

val remoteDatasourceModule = module {
    single { createOkHttpClient(androidApplication()) }
    single { createWebService<TasksService>(get(), BuildConfig.BASE_URL) }
}

fun createOkHttpClient(context: Context): OkHttpClient {
    return OkHttpClient.Builder()
            .addInterceptor(InternetConnectionInterceptor(context))
            .connectTimeout(60L, TimeUnit.SECONDS)
            .readTimeout(60L, TimeUnit.SECONDS)
            .build()
}

inline fun <reified T> createWebService(okHttpClient: OkHttpClient, baseUrl: String): T {
    val retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
    return retrofit.create(T::class.java)
}