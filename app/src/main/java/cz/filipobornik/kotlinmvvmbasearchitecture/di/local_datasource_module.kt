package cz.filipobornik.kotlinmvvmbasearchitecture.di

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import cz.filipobornik.kotlinmvvmbasearchitecture.data.local.prefs.AppPreferenceHelper
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module
import android.arch.persistence.room.Room
import cz.filipobornik.kotlinmvvmbasearchitecture.data.local.db.AppDatabase

private const val DATABASE_NAME = "tasks_database"

val localDatasourceModule = module {
    single { AppPreferenceHelper(getSharedPreferences(androidApplication())) }
    single { createDatabase(androidApplication()).tasksDaoAccess() }
}

private fun getSharedPreferences(applicationContext: Context): SharedPreferences {
    return PreferenceManager.getDefaultSharedPreferences(applicationContext)
}

private fun createDatabase(applicationContext: Context): AppDatabase {
    return Room.databaseBuilder(applicationContext,
            AppDatabase::class.java, DATABASE_NAME)
            .build()
}