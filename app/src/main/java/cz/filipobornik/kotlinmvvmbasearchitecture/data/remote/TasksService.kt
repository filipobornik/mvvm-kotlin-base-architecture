package cz.filipobornik.kotlinmvvmbasearchitecture.data.remote

import cz.filipobornik.kotlinmvvmbasearchitecture.data.model.Task
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path

interface TasksService {

    @GET("/todos")
    fun tasks(): Deferred<List<Task>>

    @GET("/todos/{id}")
    fun taskDetail(@Path(value = "id") id: Int): Deferred<Task>

}