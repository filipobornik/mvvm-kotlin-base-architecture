package cz.filipobornik.kotlinmvvmbasearchitecture.data.local.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import cz.filipobornik.kotlinmvvmbasearchitecture.data.model.Task

@Database(entities = [Task::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    public abstract fun tasksDaoAccess(): TasksDao

}