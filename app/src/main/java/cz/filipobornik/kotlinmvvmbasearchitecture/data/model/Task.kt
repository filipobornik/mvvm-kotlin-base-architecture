package cz.filipobornik.kotlinmvvmbasearchitecture.data.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.squareup.moshi.Json

@Entity
data class Task(
        @Json (name = "userId") val userId: Int,
        @NonNull @PrimaryKey @Json(name = "id") val id: Int,
        @Json(name = "title") var title: String,
        @Json(name = "completed") var completed: Boolean
) {
    val shareString get() = "Task: $title\nCompleted: $completed\nCreated by user: $userId"
}

