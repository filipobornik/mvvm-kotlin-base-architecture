package cz.filipobornik.kotlinmvvmbasearchitecture.data.local.prefs

import android.content.SharedPreferences

class AppPreferenceHelper(
        private val preferences: SharedPreferences
) {

    private val PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_TASK_ID"

    var currentTaskId: String?
        get() = preferences.getString(PREF_KEY_CURRENT_USER_ID, null)
        set(taskId) { preferences.edit().putString(PREF_KEY_CURRENT_USER_ID, taskId).apply() }

}