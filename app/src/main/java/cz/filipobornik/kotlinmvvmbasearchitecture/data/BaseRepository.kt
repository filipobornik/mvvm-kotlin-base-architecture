package cz.filipobornik.kotlinmvvmbasearchitecture.data

import cz.filipobornik.kotlinmvvmbasearchitecture.common.SingleLiveEvent

open class BaseRepository {

    val errors: SingleLiveEvent<Throwable> = SingleLiveEvent()

    protected suspend fun <T> handleErrorIfOccurs(getData: suspend () -> T) {
        try {
            getData()
        } catch (t: Throwable) {
            errors.postValue(t)
        }
    }

}