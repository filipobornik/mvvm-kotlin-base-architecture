package cz.filipobornik.kotlinmvvmbasearchitecture.data.local.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import cz.filipobornik.kotlinmvvmbasearchitecture.data.model.Task

@Dao
interface TasksDao {

    @Insert
    fun insertTask(task: Task)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTasks(tasks: List<Task>)

    @Query ("SELECT * FROM Task WHERE id = :taskId")
    fun fetchTask(taskId: Int): LiveData<Task>

    @Query ("SELECT * FROM Task")
    fun fetchTasks(): LiveData<List<Task>>

    @Update
    fun updateTask(task: Task)

    @Delete
    fun deleteMovie(task: Task)

}