package cz.filipobornik.kotlinmvvmbasearchitecture.data.model.others

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}