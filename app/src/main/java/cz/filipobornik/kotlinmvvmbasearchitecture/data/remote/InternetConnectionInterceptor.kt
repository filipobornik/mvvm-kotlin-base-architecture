package cz.filipobornik.kotlinmvvmbasearchitecture.data.remote

import android.content.Context
import cz.filipobornik.kotlinmvvmbasearchitecture.utils.exceptions.NoInternetConnectionException
import cz.filipobornik.kotlinmvvmbasearchitecture.utils.network.NetworkUtils
import okhttp3.Interceptor
import okhttp3.Response

class InternetConnectionInterceptor(
    private val context: Context
): Interceptor {

    @Throws(NoInternetConnectionException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!NetworkUtils.isInternetConnectionAvailable(context)) {
            throw NoInternetConnectionException()
        }

        val builder = chain.request().newBuilder()
        return chain.proceed(builder.build())
    }

}