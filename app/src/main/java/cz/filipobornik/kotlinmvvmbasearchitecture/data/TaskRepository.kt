package cz.filipobornik.kotlinmvvmbasearchitecture.data

import android.arch.lifecycle.LiveData
import cz.filipobornik.kotlinmvvmbasearchitecture.data.local.db.TasksDao
import cz.filipobornik.kotlinmvvmbasearchitecture.data.local.prefs.AppPreferenceHelper
import cz.filipobornik.kotlinmvvmbasearchitecture.data.model.Task
import cz.filipobornik.kotlinmvvmbasearchitecture.data.remote.TasksService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class TaskRepository(
        private val tasksService: TasksService,
        private val preferenceHelper: AppPreferenceHelper,
        private val tasksDaoAccess: TasksDao
) : BaseRepository() {

    private val tasks by lazy {
        tasksDaoAccess.fetchTasks()
    }

    fun getAllTasks(): LiveData<List<Task>> {
        return tasks
    }

    suspend fun fetchNewTasks() {
        handleErrorIfOccurs {
            val fetchedTasks = tasksService.tasks().await()
            tasksDaoAccess.insertTasks(fetchedTasks)
        }
    }

     fun getTask(taskId: Int): LiveData<Task> {
        return tasksDaoAccess.fetchTask(taskId)
    }

    suspend fun fetchTask(taskId: Int) {
        handleErrorIfOccurs {
            val task = tasksService.taskDetail(taskId).await()
            tasksDaoAccess.updateTask(task)
        }
    }

    suspend fun updateTask(task: Task) {
        tasksDaoAccess.updateTask(task)
    }

    fun setCurrentTaskId(currentTaskId: String) {
        preferenceHelper.currentTaskId = currentTaskId
    }

    suspend fun getTaskFromDb(taskId: Int): LiveData<Task> {
        return GlobalScope.async {
            tasksDaoAccess.fetchTask(taskId)
        }.await()
    }

}