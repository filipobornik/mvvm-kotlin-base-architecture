package cz.filipobornik.kotlinmvvmbasearchitecture.utils.exceptions

import java.io.IOException

class NoInternetConnectionException: IOException() {

    override fun getLocalizedMessage(): String {
        return "No Internet connection exception"
    }

}