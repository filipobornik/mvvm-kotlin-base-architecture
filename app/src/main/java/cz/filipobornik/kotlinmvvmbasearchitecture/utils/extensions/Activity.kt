package cz.filipobornik.kotlinmvvmbasearchitecture.utils.extensions

import android.content.Context
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import cz.filipobornik.kotlinmvvmbasearchitecture.R
import cz.filipobornik.kotlinmvvmbasearchitecture.utils.exceptions.NoInternetConnectionException
import retrofit2.HttpException

fun AppCompatActivity.showMessage(@StringRes messageId: Int, duration: Int = Toast.LENGTH_SHORT) {
    val message = resources.getString(messageId)
    showMessage(message, duration)
}

fun AppCompatActivity.showMessage(message: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, duration).show()
}

inline fun AppCompatActivity.setFragment(containerViewId: Int, f: () -> Fragment): Fragment? {
    val manager = supportFragmentManager
    val fragment = manager?.findFragmentById(containerViewId)
    fragment?.let { return it }
    return f().apply { manager?.beginTransaction()?.add(containerViewId, this)?.commit() }
}

inline fun AppCompatActivity.replaceFragment(containerViewId: Int, f: () -> Fragment): Fragment? {
    return f().apply { supportFragmentManager?.beginTransaction()?.replace(containerViewId, this)?.commit() }
}

fun AppCompatActivity.hideKeyboard() {
    val view = this.currentFocus
    if (view != null) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}