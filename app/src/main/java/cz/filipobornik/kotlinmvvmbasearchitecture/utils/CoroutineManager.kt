package cz.filipobornik.kotlinmvvmbasearchitecture.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CoroutineManager {


    public fun<T> asynch(block: suspend () -> T) {
        GlobalScope.launch {
            block()
        }
    }

    public fun<T> asynchAwait(block: suspend CoroutineScope.() -> T) {
        GlobalScope.launch {
            block()
        }
    }

    public fun cancellAllCoroutines() {

    }

}