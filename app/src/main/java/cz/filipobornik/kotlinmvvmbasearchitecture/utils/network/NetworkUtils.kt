package cz.filipobornik.kotlinmvvmbasearchitecture.utils.network

import android.content.Context
import android.net.ConnectivityManager

class NetworkUtils {

    companion object {
        fun isInternetConnectionAvailable(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = connectivityManager.activeNetworkInfo
            return netInfo != null && netInfo.isConnected
        }
    }

}